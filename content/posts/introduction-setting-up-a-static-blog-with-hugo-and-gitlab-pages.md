+++
date = 2019-12-28T15:00:00Z
draft = true
slug = "static-blog-hugo-gitlab-introduction"
tags = []
title = "Setting up a static blog with Hugo and GitLab Pages - Introduction"

+++
## Table of Contents

This is a series on how to build a static blog using Hugo. The series is broken down into several parts:

1. Introduction
2. Creating a new Hugo site
3. Hosting Hugo on GitLab Pages with SSL
4. Using Forestry.io and Cloudinary for content management

## Prerequisites

You should have the following installed:

* [Hugo](https://gohugo.io/ "gohugo.io")
* Git

There is minimal background required besides a working knowledge of Git / GitLab, Markdown and an internet connection.

## Architecture

The basic flow looks like this:

Write post -> Forestry.io commits changes -> GitLab CI triggers a Hugo build -> GitLab Pages serves the static site 

Because the pages are written in Markdown and are simply files, it gives you the flexibility to edit & create posts however you prefer. 

The overall benefits of static site generators (SSG) are outside the scope of this article, but here is a [short write-up](https://gohugo.io/about/benefits/ "The Benefits of Static Site Generators") from Hugo for additional context.

***

**Next:** Creating a new Hugo site
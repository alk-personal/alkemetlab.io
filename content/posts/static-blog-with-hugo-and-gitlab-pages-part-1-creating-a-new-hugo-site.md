+++
date = 2019-12-01T15:24:00Z
draft = true
slug = "static-blog-hugo-gitlab-part1"
tags = []
title = "Setting up a static blog with Hugo and GitLab Pages - Part 1: Creating a new Hugo site"

+++
## Table of Contents

This is a series on how to build a static blog using Hugo. The series is broken down into several parts:

1. Introduction
2. Creating a new Hugo site
3. Hosting Hugo on GitLab Pages with SSL
4. Using Forestry.io and Cloudinary for content management

## Generating a Hugo site

## Hugo directory structure

## Hugo configuration

## Adding a theme

***

**Next:** Hosting Hugo on GitLab Pages with SSL